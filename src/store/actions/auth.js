import {LOGIN_SUCCESS,LOGIN_FAIL} from './ActionsTypes';
import * as Google from 'expo-google-app-auth';

//SignIn User

export const signIn = () => async dispatch => {
    const androidSecret = '52737016908-3ou1c7or8bt8uld6mnqnubf6ivgvr2p4.apps.googleusercontent.com';
    const iosSecret = '52737016908-il6ja4ppgljp7qgmeuign705mhibehbb.apps.googleusercontent.com';
    try {
        const result = await Google.logInAsync({
            androidClientId: androidSecret,
            iosClientId: iosSecret,
            scopes: ['profile', 'email'],
        });
        if (result.type === 'success') {
            const name = result.user.name;
            console.log(name)
            dispatch({type: LOGIN_SUCCESS, payload: name})

        } else {
            console.log('cancelled')
        }
    } catch (e) {
        dispatch({type: LOGIN_FAIL})
        console.log(e)
    }
}