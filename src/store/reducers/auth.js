import {LOGIN_SUCCESS,LOGIN_FAIL} from '../actions/ActionsTypes';

const INITIAL_STATE = {
    signedIn: false,
    name: null
};

export default function (state = INITIAL_STATE, action) {
    const {type, payload} = action;
    switch (type) {
        case LOGIN_SUCCESS:
            console.log('name',payload)
            return {
                ...state,
                signedIn: true,
                name: payload
            };
        case LOGIN_FAIL:
            return {
                ...state,
                signedIn: false,
                name: null
            }
        default:
            return state
    }
}