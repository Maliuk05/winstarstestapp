import React,{useState} from 'react';
import {
    View,
    TextInput,
    Picker, TouchableOpacity, Image, Text,
} from 'react-native';

import {styles} from '../../styles/FormTracking/FormTracking';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import moment from "moment";
import clock from "../../images/clock.png";

const FormTracking = () => {


    //@TODO quickly workaround for example work in real life Im use iteration and dynamic index,but now I used local state
    //item1
    const [textInput1, setTextInput1] = useState('');
    const [selectedValue1, setSelectedValue1] = useState("Work");
    const [isTimePicker1,setTimePicker1] = useState(null)
    //item2
    const [textInput2, setTextInput2] = useState('');
    const [selectedValue2, setSelectedValue2] = useState("Education");
    const [isTimePicker2,setTimePicker2] = useState(null)
    //item3
    const [textInput3, setTextInput3] = useState('');
    const [selectedValue3, setSelectedValue3] = useState("Research");
    const [isTimePicker3,setTimePicker3] = useState(null)
    //item4
    const [textInput4, setTextInput4] = useState('');
    const [selectedValue4, setSelectedValue4] = useState("Meeting");
    const [isTimePicker4,setTimePicker4] = useState(null)
    //item5
    const [textInput5, setTextInput5] = useState('');
    const [selectedValue5, setSelectedValue5] = useState("Travel Jobs");
    const [isTimePicker5,setTimePicker5] = useState(null)
    //item6
    const [textInput6, setTextInput6] = useState('');
    const [selectedValue6, setSelectedValue6] = useState("Work");
    const [isTimePicker6,setTimePicker6] = useState(null)

    //time
    const [isTimePickerVisible,setIsTimePickerVisible] = useState(false);
    const [isTimePickerVisible1,setIsTimePickerVisible1] = useState(false);
    const [isTimePickerVisible2,setIsTimePickerVisible2] = useState(false);
    const [isTimePickerVisible3,setIsTimePickerVisible3] = useState(false);
    const [isTimePickerVisible4,setIsTimePickerVisible4] = useState(false);
    const [isTimePickerVisible5,setIsTimePickerVisible5] = useState(false);
    const [isTimePickerVisible6,setIsTimePickerVisible6] = useState(false);

    const hideTimePicker = () => {
        setIsTimePickerVisible(false);
    }
    const handleTimeConfirm1 = (time) => {
        setIsTimePickerVisible1(false);
        const selectTime = moment(time).format('h')
        setTimePicker1(selectTime);
    }
    const handleTimeConfirm2 = (time) => {
        setIsTimePickerVisible2(false);
        const selectTime = moment(time).format('h')
        setTimePicker2(selectTime);
    }
    const handleTimeConfirm3 = (time) => {
        setIsTimePickerVisible3(false);
        const selectTime = moment(time).format('h')
        setTimePicker3(selectTime);
    }
    const handleTimeConfirm4 = (time) => {
        setIsTimePickerVisible4(false);
        const selectTime = moment(time).format('h')
        setTimePicker4(selectTime);
    }
    const handleTimeConfirm5 = (time) => {
        setIsTimePickerVisible5(false);
        const selectTime = moment(time).format('h')
        setTimePicker5(selectTime);
    }
    const handleTimeConfirm6 = (time) => {
        setIsTimePickerVisible6(false);
        const selectTime = moment(time).format('h')
        setTimePicker6(selectTime);
    }

    return (
        <View>
            <View style={styles.item}>
                {/*@TODO in real life I has been use,for example arr.map */}
                <DateTimePickerModal
                    isVisible={isTimePickerVisible1}
                    mode="time"
                    display='spinner'
                    onConfirm={handleTimeConfirm1}
                    onCancel={()=>hideTimePicker()}
                />
                <Picker
                    mode={'dropdown'}
                    style={styles.picker}
                    selectedValue={selectedValue1}
                    onValueChange={(itemValue, itemIndex) => setSelectedValue1(itemValue)}
                >
                    <Picker.Item label="Work" value="work" />
                    <Picker.Item label="Education" value="education" />
                    <Picker.Item label="Research" value="Research" />
                    <Picker.Item label="Meeting" value="Meeting" />
                    <Picker.Item label="Travel Jobs" value="Travel Jobs" />
                </Picker>
                <View style={{
                    borderBottomWidth:1,
                    width:'70%',
                    borderColor:'#B3B3B3',
                    flexDirection:'row'
                }}>
                    <TextInput
                        style={styles.textInput}
                        placeholder={'Edits on task number 2'}
                        onChangeText={text => setTextInput1(text)}
                        defaultValue={textInput1}
                        multiline
                        returnKeyType='done'
                    />
                    <TouchableOpacity
                        onPress={()=>setIsTimePickerVisible1(!isTimePickerVisible1)}
                        style={{
                            marginTop: 18,
                            marginLeft:40
                        }}
                    >
                        { isTimePicker1 === null ? (
                            <Image
                                source={clock}
                            />
                        ) : (
                            <Text style={{
                                color: '#1E43A5'
                            }}>{isTimePicker1} h</Text>
                        )

                        }
                    </TouchableOpacity>
                </View>
            </View>
            <View style={styles.item}>
                <DateTimePickerModal
                    isVisible={isTimePickerVisible2}
                    mode="time"
                    display='spinner'
                    onConfirm={handleTimeConfirm2}
                    onCancel={()=>hideTimePicker()}
                />
                <Picker
                    mode={'dropdown'}
                    style={styles.picker}
                    selectedValue={selectedValue2}
                    onValueChange={(itemValue, itemIndex) => setSelectedValue2(itemValue)}
                >
                    <Picker.Item label="Work" value="work" />
                    <Picker.Item label="Education" value="education" />
                    <Picker.Item label="Research" value="Research" />
                    <Picker.Item label="Meeting" value="Meeting" />
                    <Picker.Item label="Travel Jobs" value="Travel Jobs" />
                </Picker>
                <View style={{
                    borderBottomWidth:1,
                    width:'70%',
                    borderColor:'#B3B3B3',
                    flexDirection:'row'
                }}>
                    <TextInput
                        style={styles.textInput}
                        placeholder={'Edits on task number 2'}
                        onChangeText={text => setTextInput2(text)}
                        defaultValue={textInput2}
                        multiline
                        returnKeyType='done'
                    />
                    <TouchableOpacity
                        onPress={()=>setIsTimePickerVisible2(!isTimePickerVisible2)}
                        style={{
                            marginTop: 18,
                            marginLeft:40
                        }}
                    >
                        { isTimePicker2 === null ? (
                            <Image
                                source={clock}
                            />
                        ) : (
                            <Text style={{
                                color: '#1E43A5'
                            }}>{isTimePicker2} h</Text>
                        )

                        }
                    </TouchableOpacity>
                </View>
            </View>
            <View style={styles.item}>
                <DateTimePickerModal
                    isVisible={isTimePickerVisible3}
                    mode="time"
                    display='spinner'
                    onConfirm={handleTimeConfirm3}
                    onCancel={()=>hideTimePicker()}
                />
                <Picker
                    mode={'dropdown'}
                    style={styles.picker}
                    selectedValue={selectedValue3}
                    onValueChange={(itemValue, itemIndex) => setSelectedValue3(itemValue)}
                >
                    <Picker.Item label="Work" value="work" />
                    <Picker.Item label="Education" value="education" />
                    <Picker.Item label="Research" value="Research" />
                    <Picker.Item label="Meeting" value="Meeting" />
                    <Picker.Item label="Travel Jobs" value="Travel Jobs" />
                </Picker>
                <View style={{
                    borderBottomWidth:1,
                    width:'70%',
                    borderColor:'#B3B3B3',
                    flexDirection:'row'
                }}>
                    <TextInput
                        style={styles.textInput}
                        placeholder={'Edits on task number 2'}
                        onChangeText={text => setTextInput3(text)}
                        defaultValue={textInput3}
                        multiline
                        returnKeyType='done'
                    />
                    <TouchableOpacity
                        onPress={()=>setIsTimePickerVisible3(!isTimePickerVisible3)}
                        style={{
                            marginTop: 18,
                            marginLeft:40
                        }}
                    >
                        { isTimePicker3 === null ? (
                            <Image
                                source={clock}
                            />
                        ) : (
                            <Text style={{
                                color: '#1E43A5'
                            }}>{isTimePicker3} h</Text>
                        )

                        }
                    </TouchableOpacity>
                </View>
            </View>
            <View style={styles.item}>
                <DateTimePickerModal
                    isVisible={isTimePickerVisible4}
                    mode="time"
                    display='spinner'
                    onConfirm={handleTimeConfirm4}
                    onCancel={()=>hideTimePicker()}
                />
                <Picker
                    mode={'dropdown'}
                    style={styles.picker}
                    selectedValue={selectedValue4}
                    onValueChange={(itemValue, itemIndex) => setSelectedValue4(itemValue)}
                >
                    <Picker.Item label="Work" value="work" />
                    <Picker.Item label="Education" value="education" />
                    <Picker.Item label="Research" value="Research" />
                    <Picker.Item label="Meeting" value="Meeting" />
                    <Picker.Item label="Travel Jobs" value="Travel Jobs" />
                </Picker>
                <View style={{
                    borderBottomWidth:1,
                    width:'70%',
                    borderColor:'#B3B3B3',
                    flexDirection:'row'
                }}>
                    <TextInput
                        style={styles.textInput}
                        placeholder={'Edits on task number 2'}
                        multiline
                        onChangeText={text => setTextInput4(text)}
                        defaultValue={textInput4}
                        returnKeyType='done'
                    />
                    <TouchableOpacity
                        onPress={()=>setIsTimePickerVisible4(!isTimePickerVisible4)}
                        style={{
                            marginTop: 18,
                            marginLeft:40
                        }}
                    >
                        { isTimePicker4 === null ? (
                            <Image
                                source={clock}
                            />
                        ) : (
                            <Text style={{
                                color: '#1E43A5'
                            }}>{isTimePicker4} h</Text>
                        )

                        }
                    </TouchableOpacity>
                </View>
            </View>
            <View style={styles.item}>
                <DateTimePickerModal
                    isVisible={isTimePickerVisible5}
                    mode="time"
                    display='spinner'
                    onConfirm={handleTimeConfirm5}
                    onCancel={()=>hideTimePicker()}
                />
                <Picker
                    mode={'dropdown'}
                    style={styles.picker}
                    selectedValue={selectedValue5}
                    onValueChange={(itemValue, itemIndex) => setSelectedValue5(itemValue)}
                >
                    <Picker.Item label="Work" value="work" />
                    <Picker.Item label="Education" value="education" />
                    <Picker.Item label="Research" value="Research" />
                    <Picker.Item label="Meeting" value="Meeting" />
                    <Picker.Item label="Travel Jobs" value="Travel Jobs" />
                </Picker>
                <View style={{
                    borderBottomWidth:1,
                    width:'70%',
                    borderColor:'#B3B3B3',
                    flexDirection:'row'
                }}>
                    <TextInput
                        style={styles.textInput}
                        placeholder={'Edits on task number 2'}
                        multiline
                        onChangeText={text => setTextInput5(text)}
                        defaultValue={textInput5}
                        returnKeyType='done'
                    />
                    <TouchableOpacity
                        onPress={()=>setIsTimePickerVisible5(!isTimePickerVisible5)}
                        style={{
                            marginTop: 18,
                            marginLeft:40
                        }}
                    >
                        { isTimePicker5 === null ? (
                            <Image
                                source={clock}
                            />
                        ) : (
                            <Text style={{
                                color: '#1E43A5'
                            }}>{isTimePicker5} h</Text>
                        )

                        }
                    </TouchableOpacity>
                </View>
            </View>
            <View style={styles.item}>
                <DateTimePickerModal
                    isVisible={isTimePickerVisible6}
                    mode="time"
                    display='spinner'
                    onConfirm={handleTimeConfirm6}
                    onCancel={()=>hideTimePicker()}
                />
                <Picker
                    mode={'dropdown'}
                    style={styles.picker}
                    selectedValue={selectedValue6}
                    onValueChange={(itemValue, itemIndex) => setSelectedValue6(itemValue)}
                >
                    <Picker.Item label="Work" value="work" />
                    <Picker.Item label="Education" value="education" />
                    <Picker.Item label="Research" value="Research" />
                    <Picker.Item label="Meeting" value="Meeting" />
                    <Picker.Item label="Travel Jobs" value="Travel Jobs" />
                </Picker>
                <View style={{
                    borderBottomWidth:1,
                    width:'70%',
                    borderColor:'#B3B3B3',
                    flexDirection:'row'
                }}>
                    <TextInput
                        style={styles.textInput}
                        placeholder={'Edits on task number 2'}
                        multiline
                        onChangeText={text => setTextInput6(text)}
                        defaultValue={textInput6}
                        returnKeyType='done'
                    />
                    <TouchableOpacity
                        onPress={()=>setIsTimePickerVisible6(!isTimePickerVisible6)}
                        style={{
                            marginTop: 18,
                            marginLeft:40
                        }}
                    >
                        { isTimePicker6 === null ? (
                            <Image
                                source={clock}
                            />
                        ) : (
                            <Text style={{
                                color: '#1E43A5'
                            }}>{isTimePicker6} h</Text>
                        )

                        }
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

export default FormTracking;