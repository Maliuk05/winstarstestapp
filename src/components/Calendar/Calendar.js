import React, {useEffect, useState} from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import {styles} from '../../styles/Calendar/Calendar';

import DateTimePickerModal from "react-native-modal-datetime-picker";
import moment from "moment";
import calendar from "../../images/calendar.png";



const Calendar = () => {

    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);

    const [chosenDate, setChosenDate] = useState(null);
    const [chosenMouth, setChosenMouth] = useState(null);
    const [initialDate,setInitialDate] = useState('');
    const [initialMouth,setInitialMouth] = useState('');

    useEffect(() => {
        const now = moment.now();
        const nowDate = moment(now).format('dddd D')
        const nowMounth = moment(now).format('YYYY MMMM');
        setInitialDate(nowDate)
        setInitialMouth(nowMounth);
    },[]);

    const hidePicker = () => {
        setDatePickerVisibility(false);
    }
    const handleConfirmPicker = (date) => {
        setDatePickerVisibility(false);
        const momentDay = moment(date).format('dddd D')
        const momentMounth = moment(date).format('YYYY MMMM')
        setChosenDate(momentDay)
        setChosenMouth(momentMounth);
    }
    return (
        <View>
            <DateTimePickerModal
                isVisible={isDatePickerVisible}
                mode="date"
                onConfirm={handleConfirmPicker}
                onCancel={()=>hidePicker()}
                style={{
                    color:'red'
                }}
            />
            <View style={{flexDirection:'row'}}>
                <TouchableOpacity onPress={()=> setDatePickerVisibility(!isDatePickerVisible) }
                                  style={{marginTop:10}}
                >
                    <Image source={calendar}/>
                </TouchableOpacity>
                <View style={{marginLeft:20}}>
                    { chosenDate === null ?
                        <Text style={{fontSize:20}}>{initialDate}</Text>
                        : <Text style={{fontSize:20}}>{chosenDate}</Text> }
                    <Text style={{fontSize:12, color:'#77787C'}}>{chosenMouth === null ? initialMouth: chosenMouth }</Text>
                </View>
            </View>
        </View>
    )
}

export default Calendar;