import React from 'react';
import {TouchableOpacity,Text,View} from 'react-native';
import {styles} from "../../styles/Help/Help";

const Help = () => (
    <View style={styles.container}>
        <View>
            <Text style={styles.text}>If you have any questions,</Text>
            <Text style={styles.text}>please contact to the
                <Text style={styles.linkText}> Head Office</Text>
            </Text>
        </View>
        <TouchableOpacity style={styles.btn} onPress={()=> alert('Write alert')}>
            <Text style={styles.textBtn}>Write</Text>
        </TouchableOpacity>
    </View>
)
export default Help;