import React from 'react';
import {Image, ImageBackground, Text} from 'react-native';
import backgroundLogo from "../../images/backgroundLogo.png";
import {styles} from "../../styles/Header/Header";
import winstarsLogo from "../../images/winstarsLogo.png";

const Header = () => (
        <ImageBackground source={backgroundLogo} style={styles.backgroundLogo}>
            <Image style={{
                alignSelf: 'flex-end'
            }} source={winstarsLogo}/>
            <Text style={styles.label}>Sign in</Text>
            <Text style={styles.text}>You can only sign in with a Gmail account</Text>
        </ImageBackground>
);

export default Header;