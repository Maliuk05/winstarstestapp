import React,{useState} from 'react';
import {View,Text, FlatList,Image,TouchableOpacity,ScrollView} from 'react-native';

import { useNavigation } from '@react-navigation/native';
import {styles} from '../../styles/Menu/Menu';

//import images
import arrowForward from '../../images/arrow-forward.png';
import add from './../../images/Menu/add.png';


import itemImage1 from './../../images/Menu/itemImage1.png';
import itemImage2 from './../../images/Menu/itemImage2.png';
import itemImage3 from './../../images/Menu/itemImage3.png';
import itemImage4 from './../../images/Menu/itemImage4.png';
import itemImage5 from './../../images/Menu/itemImage5.png';
import itemImage6 from './../../images/Menu/itemImage6.png';

const Menu = () => {

    const navigation = useNavigation();
    const [itemsMenu, setItemsMenu] = useState([
        {
            id: '1',
          itemImage: itemImage1,
          itemName: 'Tasks',
          navImage: add,
          routeName: 'Tasks'
        },
        {
            id:'2',
            itemImage: itemImage2,
            itemName: 'Time tracking',
            navImage: add,
            routeName: 'Tracking'
        },
        {
            id:'3',
            itemImage: itemImage3,
            itemName: 'Day off',
            navImage: add,
            routeName: 'DayOff'
        },
        {
            id:'4',
            itemImage: itemImage4,
            itemName: 'Job offer',
            navImage: arrowForward,
            routeName: 'jobOffer'
        },
        {
            id:'5',
            itemImage: itemImage5,
            itemName: 'Bonuses',
            navImage: arrowForward,
            routeName: 'Bonuses'
        },
        {
            id:'6',
            itemImage: itemImage6,
            itemName: 'Feedback',
            navImage: arrowForward,
            routeName: 'Feedback'
        }

    ]);

    const Item = ({itemImage,itemName,navImage,routeName}) => {
        return (
            <View style={styles.itemContainer}>
                <View style={styles.itemWrapper}>
                    <View>
                        <Image source={itemImage}/>
                    </View>
                </View>
                <View style={styles.itemRoutes}>
                    <Text style={styles.itemName}>{itemName}</Text>
                    <TouchableOpacity onPress={() => navigation.navigate(routeName)}>
                        <Image source={navImage}/>
                    </TouchableOpacity>
                </View>

            </View>
        )
    }

    return (
            <View style={styles.container}>
                {
                    itemsMenu.map((item,index) => (
                        <Item key={index}
                            itemImage={item.itemImage}
                            itemName={item.itemName}
                            navImage={item.navImage}
                            routeName={item.routeName}
                        />
                    ))
                }
            </View>
    )
}

export default Menu;