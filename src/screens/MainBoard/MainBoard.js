import React,{useState} from 'react';
import {
    View,
    Text,
    Image,
    FlatList,
    ImageBackground,
    TouchableOpacity,
    ScrollView,
    Animated,
} from 'react-native';
import {useSelector} from "react-redux";
import {styles} from "../../styles/MainBoard/MainBoard";


//images
import backgroundLogo from "../../images/backgroundLogo.png";
import winstarsLogoSM from "../../images/winstarsLogoSM.png";
import arrow from '../../images/arrow.png';
import menuSM from '../../images/menuSM.png';
import star from '../../images/star.png';
import Menu from "../../components/Menu/Menu";
import Help from "../../components/Help/Help";

const MainBoard = () => {
    const name = useSelector(state => state.auth.name);
    const [userInfo, setUserInfo] = useState([
        {
            id: '1',
            name: 'Earned this month',
            value: '0 $'
        },
        {
            id: '2',
            name: 'Job Hours this month',
            value: '0 h'
        },
        {
            id: '3',
            name: 'Rate per hour',
            value: '0 $'
        },
        {
            id: '4',
            name: 'Remains Day-off',
            value: '0 d',
        },
        {
            id: '5',
            name: 'Your rating',
            value: '0.0',
            img: star
        },
    ]);
    const [showUserInfo, toggleShowUserInfo] = useState(false);

    const initialStyle=[styles.arrowInitStyle];
    const RotateStyle=[styles.arrowRotateStyle];
    const Item = ({name,value,img}) => {
        return (
            <View style={styles.itemList}>
                <Text style={styles.itemText}>{name}</Text>
                <View style={{
                    flexDirection: 'row',
                }}>
                    <Text style={styles.itemText}>{value}</Text>
                    <Image style={{marginTop:8}} source={img}/>
                </View>
            </View>
        )
    }

    return (
        <View style={styles.container}>
            <ImageBackground
                source={backgroundLogo}
                style={styles.backgroundLogo}
                // resizeMode={"contain"}
            >
                <View>
                    <View style={{flexDirection:'row',
                        paddingLeft:10
                    }}>
                        <TouchableOpacity onPress={()=>alert('Drawer navbar')}>
                            <Image source={menuSM}/>
                        </TouchableOpacity>
                        <Image style={{
                            marginLeft: 20
                        }} source={winstarsLogoSM}/>
                    </View>
                    <View style={{
                        flexDirection: 'row',
                        justifyContent: "space-between",
                        paddingTop: 30
                    }}>
                        <Text style={styles.username}>{name}</Text>
                        <TouchableOpacity
                            onPress={()=> toggleShowUserInfo(!showUserInfo)}>
                            <Animated.View style={!showUserInfo?initialStyle:RotateStyle }>
                                <Image source={arrow}
                                       tintColor={'#fff'}
                                />
                            </Animated.View>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.itemContainer}>
                    { showUserInfo ?
                        (  <FlatList
                            data={userInfo}
                            renderItem={({ item }) => (
                                <Item
                                    name={item.name}
                                    value={item.value}
                                    img={item.img}
                                />
                            )}
                            keyExtractor={item => item.id}
                        />) : null

                    }

                </View>
            </ImageBackground>
                <ScrollView style={{
                    borderTopRightRadius: 8,
                    borderTopLeftRadius: 8,
                    position: 'relative',
                    top: -10,
                }}>
                    <View style={{
                        justifyContent: 'space-around'
                    }}>
                        <View>
                            <Menu/>
                        </View>
                        <View style={{
                            paddingTop: 20,
                            paddingHorizontal:20,
                            backgroundColor: '#fff'
                        }}>
                        </View>
                    </View>

                </ScrollView>
            <View style={{
                paddingHorizontal: 20,
                paddingBottom: 40
            }}>
                { !showUserInfo ? <Help/> : null
                }
            </View>

        </View>
    )
}

export default MainBoard;