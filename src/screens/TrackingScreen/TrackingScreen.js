import React from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    Image,
    ImageBackground,
    ScrollView
} from 'react-native';
import { useNavigation } from '@react-navigation/native';

//import images
import winstarsLogoSM from '../../images/winstarsLogoSM.png';
import menusSM from '../../images/menuSM.png';
import backgroundLogo from "../../images/backgroundLogo.png";

//custom components
import {styles} from '../../styles/TrackingScreen/TrackingScreen';
import Calendar from "../../components/Calendar/Calendar";
import FormTracking from "../../components/FormTracking/FormTracking";


const TrackingScreen = () => {

    const navigation = useNavigation();

    return (
        <ScrollView style={{
            marginTop:30,
            backgroundColor:'#fff'
        }}>
        <View style={styles.container}>
            <ImageBackground style={styles.headerContainer} source={backgroundLogo}>
                <View style={styles.logoContainer}>
                    <TouchableOpacity onPress={()=>alert('Drawer nav') }>
                        <Image source={menusSM}/>
                    </TouchableOpacity>
                    <Image style={styles.logoImage} source={winstarsLogoSM}/>
                </View>
                <Text style={styles.title}>Time Tracking</Text>
            </ImageBackground>
            <View style={styles.mainContainer}>
                <Calendar/>
                <ScrollView>
                    <View style={styles.wrapperContainer}>
                        <FormTracking/>
                    </View>
                    <TouchableOpacity
                        style={styles.finishBtn}
                        onPress={()=>navigation.navigate('MainBoard')}
                    >
                        <Text style={{color:'#fff'}}>Finished work</Text>
                    </TouchableOpacity>
                </ScrollView>
            </View>
        </View>
        </ScrollView>
    )
}

export default TrackingScreen;