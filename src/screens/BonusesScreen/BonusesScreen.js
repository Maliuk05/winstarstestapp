import React from 'react';
import {View,Text} from 'react-native';

const BonusesScreen = () => {

    return (
        <View style={{
            justifyContent:'center',
            alignItems: 'center',
            flex:1
        }}>
            <Text>Bonuses Screen</Text>
        </View>
    )
}

export default BonusesScreen;