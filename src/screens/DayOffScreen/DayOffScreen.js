import React from 'react';
import {View,Text} from 'react-native';

const DayOffScreen = () => {

    return (
        <View style={{
            justifyContent:'center',
            alignItems: 'center',
            flex:1
        }}>
            <Text>Day Off Screen</Text>
        </View>
    )
}

export default DayOffScreen;