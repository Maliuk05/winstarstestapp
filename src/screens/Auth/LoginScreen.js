import React from 'react';

//styles
import {
    View,
    Text,
    Image,
    TouchableOpacity
} from 'react-native';
import {styles} from "../../styles/Auth/LoginScreen";
import googleLogo from '../../images/googleLogo.png';

//redux
import {useDispatch} from 'react-redux';
import {signIn} from '../../store/actions/auth';

//custom components
import Help from "../../components/Help/Help";
import Header from "../../components/Header/Header";


const LoginScreen = () => {
    const dispatch = useDispatch();

    return (
        <View style={styles.container}>
                <Header/>
            <View style={styles.containerInfo}>
                <TouchableOpacity
                    style={styles.googleBtn}
                    onPress={() => dispatch(signIn()) }>
                    <Image source={googleLogo}/>
                    <Text style={styles.googleBtnText}>Sign in with Google</Text>
                </TouchableOpacity>
                <View style={{
                    marginBottom: '4%'
                }}>
                    <Help/>
                </View>
            </View>
        </View>
    )
}

export default LoginScreen;