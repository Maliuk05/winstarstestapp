import React from 'react';
import {View,Text} from 'react-native';

const FeedbackScreen = () => {

    return (
        <View style={{
            justifyContent:'center',
            alignItems: 'center',
            flex:1
        }}>
            <Text>Feedback Screen</Text>
        </View>
    )
}

export default FeedbackScreen;