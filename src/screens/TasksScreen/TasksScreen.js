import React from 'react';
import {View,Text} from 'react-native';

const TasksScreen = () => {

    return (
        <View style={{
            justifyContent:'center',
            alignItems: 'center',
            flex:1
        }}>
                <Text>Task screen</Text>
        </View>
    )
}

export default TasksScreen;