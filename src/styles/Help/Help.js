import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
    container: {
     flexDirection: 'row',
     justifyContent: 'space-between',
    },
   text: {
       color: '#828385'
   },
    linkText: {
        color:'#1E43A5'
    },
    btn: {
        backgroundColor: '#1E43A5',
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 10,
        paddingHorizontal: 30
    },
    textBtn: {
     color: '#fff',
     fontWeight: '700',
     fontSize: 14,
     lineHeight: 16
    }
});