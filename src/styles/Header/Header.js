import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
    backgroundLogo: {
        marginTop: '7%',
        paddingRight: '5%',
        flex: 1,
        backgroundColor: '#1E43A5',
        resizeMode: "cover",
        justifyContent: "center",
    },
    label: {
        color: '#fff',
        fontSize: 18,
        marginLeft: 30,
        marginTop: 10,
    },
    text: {
        color: '#fff',
        fontSize: 14,
        marginLeft: 30,
    },
});