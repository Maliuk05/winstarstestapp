import {StyleSheet,Dimensions} from 'react-native';

export const styles = StyleSheet.create({
   container: {
       flex:1,
       backgroundColor: '#fff'
   },
    backgroundLogo: {
        marginTop: '7%',
        paddingTop:40,
        backgroundColor: '#1E43A5',
        justifyContent: "space-between",
    },
    containerInfo: {
       flex:2,
    },
    username: {
      color: "#fff",
      paddingLeft: '10%',
      fontSize: 16,
      lineHeight: 32,
    },
    itemContainer: {
        backgroundColor: 'rgba(255, 255, 255, 0.25)',
       marginTop: 10,
       borderTopLeftRadius: 8,
        borderTopRightRadius: 8,
        paddingBottom: 14,
        justifyContent: 'center',
    },
    itemList: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 30,
        paddingVertical: 5,
        alignContent: 'center',
    },
    itemText: {
       color: '#fff',
       lineHeight: 32,
       fontSize: 14,
       fontWeight: '500'
    },
    arrowInitStyle:{
        marginRight:20,
        marginTop:5
    },
    arrowRotateStyle: {
        marginRight:20,
        marginTop:5,
        transform: [{ rotate: "180deg" }]
    }
});