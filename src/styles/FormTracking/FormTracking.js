import {StyleSheet} from 'react-native';

export const styles = {
    item:{
        flexDirection: 'row'
    },
    picker: {
        width:'40%'
    },
    textInput: {
        marginTop: 10,
        fontSize: 14,
        lineHeight: 32,
        color: '#535353'
    },
    date: {
        fontSize: 20,
        lineHeight:32,
    },

}