import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
   container: {
       flex:1,
       paddingBottom: 10
   },
    containerInfo: {
       paddingTop: 30,
       flex: 3,
        marginLeft:'5%',
        marginRight: '5%',
        justifyContent: 'space-between',
    },
    googleBtn: {
       flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 20,
        borderWidth:1,
        borderRadius: 5,
        borderColor: '#AAAAAA',
        height: 60,
    },
    googleBtnText: {
       paddingLeft: 30,
       fontSize: 14,
    }
});