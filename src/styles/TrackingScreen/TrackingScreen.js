import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
    container: {
        // marginTop: 30,
        // paddingTop:30,
        flex: 1,
        backgroundColor: '#1E43A5'
    },
    headerContainer: {
        paddingHorizontal: '6%',
        flex:1,
        backgroundColor:'#1E43A5',
        justifyContent: 'space-around'
    },
    logoContainer: {
      paddingTop:30,
      flexDirection: 'row',
    },
    logoImage: {
      marginLeft: 15
    },
    title: {
        paddingTop:10,
        paddingBottom:10,
        fontSize:16,
        marginLeft: 16,
        color: '#fff',
        lineHeight: 32
    },
    mainContainer: {
        flex:4,
        paddingTop: 40,
        paddingHorizontal: '5%',
        position:'relative',
        backgroundColor: '#fff',
        borderTopLeftRadius:10,
        borderTopRightRadius:10,
    },
    wrapperContainer: {
        paddingTop: 40
    },
    finishBtn: {
        marginTop: 32,
        paddingHorizontal:22,
        paddingVertical:17,
        backgroundColor:'#1E43A5',
        borderRadius:16,
        width: '40%',
        alignSelf:'flex-end',
        alignItems:'center'
    }
})