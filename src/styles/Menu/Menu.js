import {StyleSheet,Dimensions} from 'react-native';

const {width: WIDTH, height: HEIGHT} = Dimensions.get('window')



export const styles = StyleSheet.create({
    container: {
        paddingHorizontal: "5%",
        backgroundColor: '#fff',
        flex:1,
        borderTopRightRadius: 8,
        borderTopLeftRadius: 8,
        paddingTop: '5%',
    },
    itemContainer: {
        // backgroundColor: 'red',
        flexDirection: 'row',
        // justifyContent: 'space-between'
    },
    itemWrapper: {
        paddingTop:'3%',
        flexDirection: 'row',
        alignItems: 'center'
    },
    itemRoutes: {
        marginLeft: 20,
        alignItems: 'center',
        width: '88%',
        marginTop:12,
        borderBottomWidth:1,
        borderColor: '#B3B3B3',
        // backgroundColor: 'black',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    itemName:{
        marginVertical:10
    }
});