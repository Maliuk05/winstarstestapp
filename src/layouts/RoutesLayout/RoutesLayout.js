import React from 'react';

//redux
import {useSelector} from 'react-redux'

//navigation
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

//Custom components
import LoginScreen from "../../screens/Auth/LoginScreen";
import MainBoard from "../../screens/MainBoard/MainBoard";
import TasksScreen from "../../screens/TasksScreen/TasksScreen";
import TrackingScreen from "../../screens/TrackingScreen/TrackingScreen";
import DayOffScreen from "../../screens/DayOffScreen/DayOffScreen";
import JobOfferScreen from "../../screens/JobOfferScreen/JobOfferScreen";
import BonusesScreen from "../../screens/BonusesScreen/BonusesScreen";
import FeedbackScreen from "../../screens/FeedbackScreen/FeedbackScreen";

const Stack = createStackNavigator();

const RoutesLayout = () => {

    const isSignIn = useSelector(state=> state.auth.signedIn)

    const {Screen,Navigator}  = Stack;
    return (
        <NavigationContainer>
            <Navigator mode="modal" headerMode="none">
                {
                !isSignIn ? (
                    <>
                        <Screen name="Login" component={LoginScreen} mode='modal' headerMode='none'/>
                    </>
                ) : (
                  <>
                           <Screen name='MainBoard' component={MainBoard}/>
                           <Screen name='Tracking' component={TrackingScreen}/>
                           <Screen name='Tasks' component={TasksScreen}/>
                           <Screen name='DayOff' component={DayOffScreen}/>
                           <Screen name='jobOffer' component={JobOfferScreen}/>
                           <Screen name='Bonuses' component={BonusesScreen}/>
                           <Screen name='Feedback' component={FeedbackScreen}/>
                  </>
                )
                }
            </Navigator>
        </NavigationContainer>
    )
}

export default RoutesLayout;