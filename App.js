import React from 'react';
import RoutesLayout from "./src/layouts/RoutesLayout/RoutesLayout";

//redux
import {Provider} from 'react-redux';
import store from './src/store/store';

const App = () => {

    return (
        <Provider store={store}>
            <RoutesLayout/>
        </Provider>
  );

}

export default App;